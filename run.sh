
if [[ ! -d "1m_6ex_karel" ]]
then
  echo "Downloading data"
  wget https://rhgt8w.bn.files.1drv.com/y4mlPNtWRxNnyxHUQMq0LJu-wQ5T708OEsDl-eJ1NFdRMsgxGp8DeQNqdJklob0lakwJbepphas2qzQpvfbnHo68tfeoptZ9X9-YUZSGp7SxA_1JPnUf-KnegM-ABeiOaRSkP4M5RmunS3l-q2Db0PDFzwc0vVY3gbFFwB7stv5nK8FRN37sHl5oH37myAAm_1L-AXMetpigZUCOOlwQVbXXw -O karel.tar.gz
fi
echo "Lets look at the data"
tar -zvxf karel.tar.gz --to-stdout 1m_6ex_karel/train.json | head -n 3
echo "Around mil strings"

echo "Getting train data"
if [[ ! -d "1m_6ex_karel" ]]
then
  tar -xzvf karel.tar.gz
fi
cat ./1m_6ex_karel/train.json | head -n 10000 > 1prcnt.json
cat ./1m_6ex_karel/train.json | head -n 30000 > 3prcnt.json
cat ./1m_6ex_karel/train.json | head -n 100000 > 10prcnt.json

echo "Installing dependencies(cython) and installing"
pip2 install cython
python2 setup.py install

echo "Removing directories in case we restart experiment"
rm -r exps/supervised_use_grammar
rm -r exps/supervised_learn_grammar
rm -r exps/reinforce_finetune
rm -r exps/beamrl_finetune

# Train a simple supervised model, using the handcoded syntax checke
python2 ./cmds/train_cmd.py --kernel_size 3 \
             --conv_stack "64,64,64" \
             --fc_stack "512" \
             --tgt_embedding_size 256 \
             --lstm_hidden_size 256 \
             --nb_lstm_layers 2 \
             \
             --signal supervised \
             --nb_ios 5 \
             --nb_epochs 5 \
             --optim_alg Adam \
             --batch_size 128 \
             --learning_rate 1e-4 \
             \
             --train_file ./1prcnt.json \
             --val_file ./1m_6ex_karel/val.json \
             --vocab ./1m_6ex_karel/new_vocab.vocab \
             --result_folder exps/supervised_use_grammar \
             \
             --use_grammar \
              \
               --use_cuda


python2 ./cmds/train_cmd.py --kernel_size 3 \
             --conv_stack "64,64,64" \
             --fc_stack "512" \
             --tgt_embedding_size 256 \
             --lstm_hidden_size 256 \
             --nb_lstm_layers 2 \
             \
             --signal supervised \
             --nb_ios 5 \
             --nb_epochs 5 \
             --optim_alg Adam \
             --batch_size 128 \
             --learning_rate 1e-4 \
             --beta 1e-5 \
             \
             --train_file ./1prcnt.json \
             --val_file ./1m_6ex_karel/val.json \
             --vocab ./1m_6ex_karel/new_vocab.vocab \
             --result_folder exps/supervised_learn_grammar \
             \
             --use_grammar \
              \
               --use_cuda


# Use a pretrained model, to fine-tune it using simple Reinforce
# Change the --environment flag if you want to use a reward including performance.
python2 ./cmds/train_cmd.py  --signal rl \
              --environment BlackBoxGeneralization \
              --nb_rollouts 100 \
              \
              --init_weights exps/supervised_use_grammar/Weights/best.model \
              --nb_epochs 5 \
              --optim_alg Adam \
              --learning_rate 1e-5 \
              --batch_size 16 \
              \
              --train_file ./1prcnt.json \
              --val_file ./1m_6ex_karel/val.json \
              --vocab ./1m_6ex_karel/new_vocab.vocab \
              --result_folder exps/reinforce_finetune \
              \
              --use_grammar \
               \
               --use_cuda


# Use a pretrained model, fine-tune it using BS Expected reward
python2 ./cmds/train_cmd.py  --signal beam_rl \
              --environment BlackBoxGeneralization \
              --reward_comb RenormExpected \
              --rl_inner_batch 8 \
              --rl_use_ref \
              --rl_beam 64 \
              \
              --init_weights exps/supervised_use_grammar/Weights/best.model \
              --nb_epochs 5 \
              --optim_alg Adam \
              --learning_rate 1e-5 \
              --batch_size 16 \
              \
              --train_file ./1prcnt.json \
              --val_file ./1m_6ex_karel/val.json \
              --vocab ./1m_6ex_karel/new_vocab.vocab \
              --result_folder exps/beamrl_finetune \
              \
              --use_grammar \
               \
               --use_cuda