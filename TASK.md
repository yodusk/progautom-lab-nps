### Лабораторная работа №2 по автоматизации программирования

команды в colab для запуска
* !git clone https://gitlab.com/yodusk/progautom-lab-nps.git
* !mv ./progautom-lab-nps/* ./
* !./run.sh для обучения
* !./eval.sh для оценки

Данные после запуска не удалось выгрузить даже локально из-за объема, поэтому прикладываю ссылку на колаб, где запускался эксперимент [Colab](https://colab.research.google.com/drive/1d-LRsZ3tmJkEHA_y5TgkC74DJV0QbCx5#scrollTo=vwgVuQY8I4vB)

# Повторение результатов статьи 

Повторяем эксперимент из статьи

```
Bunel, R., Hausknecht, M., Devlin, J., Singh, R., & Kohli, P. (2018). 
Leveraging grammar and reinforcement learning for neural program synthesis. 
arXiv preprint arXiv:1805.04276.
```

В статье предлагается использовать обучение с подкреплением и отсев генерируемых вариантов программ путем из их исполнения и проверки синтаксиса.

Используется сгенерированный датасет [karel_dataset](https://msr-redmond.github.io/karel-dataset/).

Исходный код для начала эксперимента доступен на [Github](https://github.com/bunelr/GandRL_for_NPS).

Для обучения моделей можно использовать Google Colab c GPU.

## Задача 1. Подготовка данных

### Задание 1. 

Получите karel_dataset, найдите в нем train.json и разберитесь в его структуре.

Вопросы
 - как правильно называется формат файла train.json?
 специфического формата не нашел, выглядит как невалидный json из-за нескольких корней
 - как взять часть из файла train.json?
 cat file | head -n num
 - подготовьте файлы корректного формата размером 1%, 3%, 10% от оригинала train.json
[+]
 
### Задание 2. 
 
Напишите код, который загружает данные из train.json
 
Вопросы
 - оцените объем необходимой RAM
    1 гб хватит, если реализовать итератор словарей, то можно еще уменьшить обьем
 - реализуйте загрузку в итератор словарей (паттерн итератор)
   можно реализовать через gensim, но не нашел версии под python 2

  
## Задача 2. Подготовка репозитория 

### Задание 1. 

Получите GandRL_for_NPS из github и смерджите его в этот репозиторий (см. [как это сделать](./FAQ.md#как-объединить-репозитории) в FAQ.md)

Вопросы
 - на каком языке реализован? 
 python2
 - что нужно сделать для установки эксперимента и зависимостей? 
 установить cython и запустить setup.py
 - где должны быть размещены данные? 
 в моей реализации они лежат в папке /1m_6ex_karel, по дефолту они лежат в папке data
 - что нужно сделать для запуска эксперимента, как указать параметры и какие значения выбрать? 
 запустить скрипт train из директории cmds с параметрами указаными в аргументах
 - что нужно сделать для проверки обученной модели? 
 запустить скрипт eval_cmd
 
### Задание 2. 
 
Загрузите 1% от train.json и остальные файлы из karel_dataset
 
Вопросы
 - зачем нужны файлы *.thdump в папке датасета?
 единственное предположение снепшот данных перед началом эксперимента
 - что содержит new_vocab?
 словарь для разметки слов
 - где находится датасет для контроля и для теста?
 в json файлах val и test
 - как устроен экземпляр данных для обучения?
{"examples": [{"actions": ["turnLeft", "move", "turnLeft", "turnRight", "turnLeft", "turnRight", "turnLeft", "turnRight", "turnLeft", "turnRight", "pickMarker"], "example_index": 0, "inpgrid_json": {"blocked": "9:3 8:13 6:0 6:8 5:0 5:6 3:10 2:9 0:10", "cols": 14, "crashed": false, "hero": "8:0:south", "markers": "9:5:1 8:1:1 8:10:1 8:11:3 7:6:1 7:7:1 7:10:1 6:5:1 4:9:4 2:0:1 1:6:3 1:7:3 0:13:1", "rows": 10}, "inpgrid_tensor": ...., "outgrid_json": {"blocked": "9:3 8:13 6:0 6:8 5:0 5:6 3:10 2:9 0:10", "cols": 14, "crashed": false, "hero": "8:1:east", "markers": "...."}, 

### Задание 3. 

Проведите эксперимент с 1% данных

Вопросы
 - как указать вид модели?
 можно указать параметры или веса
 - какие ошибки возникли при запуске и как вы их устранили? 
 ошибки с неправильными размерностями тензоров, ошибка несоответсвия типов
 - сколько эпох вы провели?
 simple model 10.
 simple model learning grammar 10.
 pre-trained reinforce 5
 pre-trained BS 5
 - где сохранены результаты и логи эксперимента?
в папке exps для каждой из модели
 - какого качества получен результат?
 Loss : 1.10711
 ValidationAccuracy : 0.000000

 
### Задание 4. 
 
Сделайте свой клон репозитория с исправлениями, добавьте архивы подготовленных данных 1%, 3%, 10% на файлообменник с доступом по прямой ссылке

https://fex.net/ru/s/pmfsyac
Вопросы
 - как получить данные по ссылке из командной строки?
 wget
 - где находится ваш репозиторий?
 gitlab

### Задание 5*.

Подключите журналирование кривых обучения и промежуточных результатов в [TensorboardX](https://github.com/lanpa/tensorboardX). 
Для этого нужно использовать SummaryWriter для сохранения промежуточных значений функции потерь внутри цикла обучения. Например, после каждого минибатча или 100 миниматчей. 

Логи сохранять в подпапку ``runs`` папки эксперимента в ``./exps/*``

Для использования установить Tensorboard и указать данную папку в качестве исходной.
  
## Задача 3. Анализ и повторение результата

### Задание 1. 

Проведите эксперименты для 1%, 3%, 10% данных для MLE (supervised), RL_beam и обучаемой и предзаданной моделью синтаксиса

Вопросы
 - сколько времени заняло проведение эксперимента, какие ресурсы использовали?
 если учитывать исправления, тесты и подготовку > 10 часов
 только время обучения > 2 часов
 colab gpu
 - какого качества удалось достичь?
 ValidationAccuracy : 1.923848.
 - приведите 10 примеров синтеза программы для karel обученной моделью
 в папке ./exps/supervised_use_grammar/Results/generated

Начните с MLE и используйте обученную таким образом модель для RL в качестве начального приближения.
См. подробнее в репозитории проекта.

### Задание 2. 

Подготовьте слайды по эксперименту: 
1. название статьи и постановка задачи (по статье, формулы)
2. использованные данные (ссылка и как готовили)
3. постановка эксперимента (репо и команды запуска)
4. таблица с результатами (сводная)

Вопросы
 - сравните с результатами для Small dataset из статьи, удалось ли повторить их результаты?
 - как влияет выбор алгоритма контроля корректности программы на качество, в зависимости от размера?


