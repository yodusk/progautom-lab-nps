python2 ./cmds/eval_cmd.py --model_weights exps/supervised_use_grammar/Weights/best.model \
            \
            --vocabulary ./1m_6ex_karel/new_vocab.vocab \
            --dataset ./1m_6ex_karel/val.json \
            --eval_nb_ios 5 \
            --eval_batch_size 8 \
            --output_path exps/supervised_use_grammar/Results/ValidationSet_ \
            \
            --beam_size 64 \
            --top_k 10 \
            --dump_programs \
            --use_grammar \
            \
            --use_cuda
# Evaluate a trained model on the test set
python2 ./cmds/eval_cmd.py --model_weights exps/beamrl_finetune/Weights/best.model \
            \
            --vocabulary ./1m_6ex_karel/new_vocab.vocab \
            --dataset ./1m_6ex_karel/test.json \
            --eval_nb_ios 5 \
            --eval_batch_size 8 \
            --output_path exps/beamrl_finetune/Results/TestSet_ \
            \
            --beam_size 64 \
            --top_k 10 \
            --use_grammar \
            \
            --use_cuda 